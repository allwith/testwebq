package cn.jbit.dao;

import cn.jbit.bean.Page;

public interface PageDao {
	//显示于本页的数据
	public Page getResult(Page page);
	//获取总行数
	public int getCount();
}
